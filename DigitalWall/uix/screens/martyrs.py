'''
Martyrs Module
==============
This screen is supposed to show a list of martyrs rotating aorund  a circle.
'''
from kivy.factory import Factory
from kivy.lang import Builder
from kivy.properties import StringProperty
from kivy.metrics import dp

from kivy.core.audio import SoundLoader
msound = SoundLoader.load('data/fighter.mp3')




class Picture(Factory.Button):
    '''Picture is the class that will show the image with a white border and a
    shadow. They are nothing here because almost everything is inside the
    picture.kv. Check the rule named <Picture> inside the file, and you'll see
    how the Picture() is really constructed and used.

    The source property will be the filename to show.
    '''

    source = StringProperty(None)

    Builder.load_string('''
<Picture>:
    # each time a picture is created, the image can delay the loading
    # as soon as the image is loaded, ensure that the center is changed
    # to the center of the screen.
    on_size: self.center = win.Window.center
    size: image.size
    size_hint: None, None
    on_release: app.load_screen(self.source)
    Image:
        id: image
        source: root.source
        pos: root.pos
        # create initial image to be 400 pixels width
        size: 320, 320 / self.image_ratio

        # add shadow background
        canvas.before:
            Color:
                rgba: 1,1,1,1
            BorderImage:
                source: 'images/shadow32.png'
                border: (36,36,36,36)
                size:(self.width+72, self.height+72)
                pos: (root.x - 36, root.y - 36)
''')


class Martyrs(Factory.Screen):
    '''Screen starts, looks for a list of folders in the directory
    `images`, fetches the image the same name as the folder + jpg
    loads them in memory and presents them in around a circle,
    floating and rotating around the circle.
    Once the User chooses a image and drags it inside the circle,
    
    - All the other images fade out.
    - The selected image zooms out.
    - Detailed time line for the selected person fades in.
    '''
    Builder.load_string('''
<Martyrs>
    name: 'martyrs'
    BoxLayout
        do_rotation: False
        id: plane
        #scale: .50
        opacity: 0
''')

    def __init__(self, **kwargs):
        super(Martyrs, self).__init__(**kwargs)
        self.last_r, self.last_t = dp(10), dp(10)
        self.populated = False

    def on_enter(self):
        if self.populated:
            return
        self.populated = True
        from kivy.clock import Clock
        Clock.schedule_once(self.load_martyrs)

    def load_martyrs(self, dt):
        import os
        lst = []
        msound.play()
        for item in os.listdir('images'):
            if not os.path.isdir('./images/'+item): continue
            item = './images/' + item + '/' + item + '.jpg'
            self.rotate_widget(Picture(source=item))

    def rotate_widget(self, widget):
        import random
        self.ids.plane.add_widget(widget)
        Animation = Factory.Animation
        Animation.cancel_all(widget)
        ln = len(widget.parent.children)
        # x, y = self.last_r, self.last_t
        # if x > self.ids.plane.width - widget.width:
        #     x = dp(10)
        #     y = self.last_t + widget.height + dp(10)
        # else:
        #     x = self.last_r + dp(30)
        # if len(widget.parent.children) > 5:
        #     x += dp(400)
        #     y -= dp(200)
        # self.last_r, self.last_t = widget.width + x, widget.y + widget.height
        # Animation(x=x, y=y, d=1).start(widget)
        Animation(opacity=1, d=1).start(self.ids.plane)
        
        

'''
"Timeline"

 ================================
|-------------------------------------------
|1937 - 1943  |  1944 - 1952  | 19|53 - 1957    <- autoscrolling timeline
|-------------------------------------------
|                                   [IMG] <- Zoomable Image
|                  [txt] <- Zoomable text
|      [Vid] <- Playable video
|
 ================================"
'''

import os
from kivy.factory import Factory
from kivy.lang import Builder
from kivy.clock import Clock

from kivy.properties import StringProperty
from kivy.metrics import dp
from kivy.app import App
app = App.get_running_app()

class MPicture(Factory.Widget):
    '''Picture is the class that will show the image with a white border and a
    shadow. They are nothing here because almost everything is inside the
    picture.kv. Check the rule named <Picture> inside the file, and you'll see
    how the Picture() is really constructed and used.

    The source property will be the filename to show.
    '''

    source = StringProperty(None)

    Builder.load_string('''
<MPicture>:
    # each time a picture is created, the image can delay the loading
    # as soon as the image is loaded, ensure that the center is changed
    # to the center of the screen.
    on_size: self.center = win.Window.center
    size: image.size
    size_hint: None, None
    #on_release: app.load_screen(self.source)
    Image:
        id: image
        source: root.source
        pos: root.pos
        # create initial image to be 400 pixels width
        size: 400, 400 / self.image_ratio

        # add shadow background
        canvas.before:
            Color:
                rgba: 1,1,1,1
            BorderImage:
                source: 'images/shadow32.png'
                border: (36,36,36,36)
                size:(self.width+72, self.height+72)
                pos: (root.x - 36, root.y - 36)
''')


class Video(Factory.Scatter):
    '''Video is the class that will show the image with a white border and a
    shadow. They are nothing here because almost everything is inside the
    picture.kv. Check the rule named <Picture> inside the file, and you'll see
    how the Picture() is really constructed and used.

    The source property will be the filename to show.
    '''

    source = StringProperty(None)

    Builder.load_string('''
<Video>:
    on_size: self.center = win.Window.center
    size: image.size
    size_hint: None, None
    VideoPlayer:
        id: image
        source: root.source
        pos: root.pos
        # create initial image to be 400 pixels width
        size: dp(900), dp(900)
        # add shadow background
        canvas.before:
            Color:
                rgba: 1,1,1,1
            BorderImage:
                source: 'images/shadow32.png'
                border: (36,36,36,36)
                size:(self.width+72, self.height+72)
                pos: (root.x - 36, root.y - 36)
''')


class TimeLineBase(Factory.Screen):
	'''Base class, every time line is supposed
	to subclass it.
	'''

	Builder.load_string('''
#:import Animation kivy.animation.Animation
<TimelineBase>
	on_touch_down: Animation.cancel_all(scrl)
	on_touch_move: Animation.cancel_all(scrl)
	on_touch_up: root.on_pre_enter()
	BoxLayout:
		orientation: 'vertical'
	    Button
	    	size_hint: 1, None
	    	height: dp(45)
	    	text: 'back'
	    	on_release: app.root.ids.screen_manager.current = 'martyrs'
		ScrollView
			id: scrl
			GridLayout
				padding: dp(180)
				spacing: dp(180)
				id: container
				cols: 3
				size_hint: 1, None
				height: self.minimum_height
''')



	def __init__(self, **kwargs):
		super(TimeLineBase, self).__init__(**kwargs)
		self.folder_name = self.name + 'jpg'
		# Each time line class should be based/subclassed from thos
		self.load_timeline_data(self.folder_name)

	def on_pre_enter(self):
		Clock.schedule_once(self.enable_animation, 1)

	def enable_animation(self, *dt):
		Animation = Factory.Animation
		scrl = self.ids.scrl
		anim = Animation(scroll_y=-0.2, d=30) + Animation(scroll_y=1.2, d=30)
		anim.repeat = True
		anim.start(scrl)

	def load_timeline_data(self, folder_name):
		'''Load images, videos and text based on a timeline.
		'''
		filename = 'images/' + folder_name.split('.')[0]
		filelist = []
		for fl in os.listdir(filename):
			fln = filename + '/' + fl
			if fl.startswith('.') or not os.path.isfile(fln):
				continue
			# file is consumable
			# check type of file
				
			ext = fln.rsplit('.')[-1]
			if ext in ('jpg', 'jpeg', 'png', 'gif'):
				self.ids.container.add_widget(MPicture(source=fln))
			# elif ext in ('mpg', 'mp4', 'avi'):
			# 	self.add_widget(Video(source=fln))


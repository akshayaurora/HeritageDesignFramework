'''This is a demo for the JNU Professors, meant to be a test for the event on 26th
Objectives of this demo::

1) Pick a Martyr, in this case our subject is Sardar Vallabh Bhai Patel.
2) Show case his life, achievements::

Design of the UI
================
The app starts, looks for a list of folders in the directory
`images`, fetches the image the same name as the folder + jpg
loads them in memory and presents them in around a circle,
floating and rotating around the circle.

Once the User chooses a image and drags it inside the circle,

- All the other images fade out.
- The selected image zooms out.
- Detailed time line for the selected person fades in.

"Timeline"

 ================================
|-------------------------------------------
|1937 - 1943  |  1944 - 1952  | 19|53 - 1957    <- autoscrolling timeline
|-------------------------------------------
|                                   [IMG] <- Zoomable Image
|                  [txt] <- Zoomable text
|      [Vid] <- Playable video
|
 ================================"
'''

import sys
import os

script_path = os.path.dirname(os.path.realpath(__file__))

# add module path for screen so they can be ynamically be imported
module_path = script_path + '/uix/screens/'
sys.path.insert(0, module_path)


from kivy.app import App
from kivy.lang import Builder


class HistoryDemo(App):

    def build(self):
        from uix.screens.martyrs import Martyrs

        return Builder.load_string('''
FloatLayout
    canvas.before:
        Color:
            rgba: 1, 1, 1, 1
        Rectangle:
            size: self.size
            pos: self.pos
    Image
        allow_stretch: True
        source: 'images/background.jpg'
    Label:
        text: 'Digital Wall of Martyrs'
    ScreenManager
        id: screen_manager
        Martyrs
''')

    def load_screen(self, source):
        source = source.split('/')[-1]
        scr = source.rsplit('/')[-1]

        sm = self.root.ids.screen_manager
        if scr in sm.screens:
            sm.current = scr
        else:
            # load screen modules dynamically
            # for example load_screen('LoginScreen')
            # will look for uix/screens/loginscreen
            # load LoginScreen 
            # module_path = scr.lower()
            # if not hasattr(self, module_path):
            #     import imp
            #     module = imp.load_module(scr, *imp.find_module(module_path))
            #     screen_class = getattr(module, scr)
            #     sc = screen_class() 
            #     sm.add_widget(sc)
            #     sm.current = sc.name
            from uix.screens.timeline import TimeLineBase
            sm.add_widget(TimeLineBase(name=scr)) 
            sm.current = scr


if __name__ == '__main__':
    HistoryDemo().run()

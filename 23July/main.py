'''
This is a demo app made for the event on 23rd July 2017.

About event:
============
The event itself is organised by members of JNU to honour our fallen heroes.
Specifically the paramveer chakra winners. 

The following specific people are being honoured here.
1)
2)
3)
4)
5)
6)
7)
8)
9)
10)
11)
12)
13)
'''


from kivy.app import App

import os
import sys

script_path = os.path.dirname(os.path.realpath(__file__))

# add module path for screen so they can be ynamically be imported
module_path = script_path + '/uix/screens/'
sys.path.insert(0, module_path)


class HeritageDemo(App):
    '''
    '''

    def build(self):
        from kivy.lang import Builder
        root = Builder.load_string('''
#:import FadeTransition kivy.uix.screenmanager.FadeTransition
ScreenManager
    transition: FadeTransition()
''')
        self.load_screen('MainMenu', root)
        return root

    def load_screen(self, screen, manager=None, store_back=True):
        '''Load the provided screen:
        arguments::
            `screen`: is the name of the screen to be loaded
            `manager`: the manager to load this screen, this defaults to
            the main class.
        '''
        store_back = False if screen == 'StartupScreen' else store_back

        manager = manager or self.root
        # load screen modules dynamically
        # for example load_screen('LoginScreen')
        # will look for uix/screens/loginscreen
        # load LoginScreen 
        module_path = screen.lower()
        if not hasattr(self, module_path):
            import imp
            module = imp.load_module(screen, *imp.find_module(module_path))
            screen_class = getattr(module, screen)
            sc = screen_class() 
            sc.from_back = not store_back
            setattr(self, module_path, sc)
            manager.add_widget(sc)

        else:
            sc = getattr(self, module_path)

        sc.from_back = not store_back
        manager.current = screen

        # if store_back:
        #     self._navigation_higherarchy.append(sc)
    
        return getattr(self, module_path)



if __name__ == '__main__':
    HeritageDemo().run()
